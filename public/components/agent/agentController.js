angular.module('app')
.controller('agentController', ['$scope', 'Socket', 'Session', '$state', '$timeout', '$http','Upload', function($scope, Socket, Session, $state, $timeout, $http, Upload) {
    
    $scope.user = Session.user.username;
    $scope.messages = [];


    var initRun = function() {
        $scope.msg_boxClick = true;
        $scope.msg_wrap = false;
    }

    initRun();

var setScrollTop = function(name){
    console.log("inside scrolltop")
            $timeout(() => {
        var container = document.getElementById(name);
        container.scrollTop = container.scrollHeight - container.clientHeight;
    });
}

var getTime = function(){
    var timestamp = moment().valueOf();
    var momentTime = moment.utc(timestamp);
    momentTime = momentTime.local().format('h:mm a DD/MM/YY');
    return momentTime;
}

    // upload on file select or drop
    $scope.upload = function (file,index) {
        // $scope.file = file;
        console.log("inside upload",$scope.file)
        Upload.upload({
            url: '/upload',
            data: {file: file, 'sender': $scope.user, 'receiver': Session.user.handler ? Session.user.handler : 'Admins', 'time': getTime()}
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            console.log(resp)
            var newMessage = {
                sender: $scope.user,
                receiver: Session.user.handler ? Session.user.handler : 'Admins',
                message: '',
                time: getTime(),
                path: 'uploads/'+resp.data.filename
            }
            console.log("newMessage",newMessage)
            $scope.messages.push(newMessage)
            // console.log($scope.popupMessage[0])
            // setScrollTop('msg_body');
        $timeout(() => {
        var container = document.getElementById('msg_body');
        container.scrollTop = container.scrollHeight - container.clientHeight;
            });
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
    };
    
    $scope.close = function(e) {
    var chatBox=document.getElementById('chatBox');
        chatBox.style.visibility = 'hidden';
        // e.stopPropogation();
    }
    $scope.disconnect = function() {
            // Session.user = '';
            Socket.emit('clearHandler',{},function(res){
                console.log(res);
            })
            Socket.emit('logout',{},function(){
                console.log("Logged out from server");
            })
            Session.user = '';
            $http.get('/logout').then(function(res){
            $state.go('login')
            console.log("LOGGED OUT")});
    }
    $scope.sendMessage = function(event) {
    var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
        // console.log("Event:",key,"user:",user,"index:",index);
        if(key == 13 && $scope.messageInput != ""){

            var timestamp = moment().valueOf();
            var momentTime = moment.utc(timestamp);
                momentTime = momentTime.local().format('h:mm a DD/MM/YY');

                if(Session.user.handler){
                    var newMessage = {
                        sender: $scope.user,
                        receiver: Session.user.handler,
                        message: $scope.messageInput,
                        time: momentTime
                        
                    }
                } else {
                    var newMessage = {
                        sender: $scope.user,
                        receiver: 'Admins',
                        message: $scope.messageInput,
                        time: momentTime
                        
                    }
                }
            Socket.emit("chatMessage", newMessage, function(response) {
                if (response == 'success') {
                    $scope.messages.push(newMessage)
                    $scope.messageInput = "";
                    setScrollTop('msg_body');
                }
            });
    }
    }
    $scope.scroll = function(){
        $scope.msg_wrap = !$scope.msg_wrap
        setScrollTop('msg_body');
    }
    $scope.getMessages = function(res) {
        console.log("getmessages")
        Socket.emit('getMessages', {}, function(messages) {
            // console.log('Messages:', messages)
            $scope.messages = messages;
            setScrollTop('msg_body');
        })
    }

    $scope.getMessages();

    Socket.on('chatMessage', function(message) {
        console.log("INCOMING MSG::",message);
        $scope.messages.push(message.data);
        $scope.msg_wrap = true;
        // console.log("messages:::::",$scope.messages);
        setScrollTop('msg_body');
    });

    Socket.on('setAdmin',function(input){
        console.log("SetAdmin input::",input);
        Session.user.handler = input;
        var newMessage = {
            sender: "System",
            message: "Admin set to " + Session.user.handler
        }
        $scope.messages.push(newMessage);
    })
}])