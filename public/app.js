'use strict';

var app = angular.module('app', [
    'ui.router',
    'ngFileUpload'
])

angular.element(document).ready(function () {
    angular.bootstrap(document, ['app']);
});