-- phpMyAdmin SQL Dump
-- version 4.6.4deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 04, 2017 at 12:11 PM
-- Server version: 5.7.17-0ubuntu0.16.10.1
-- PHP Version: 7.0.15-0ubuntu0.16.10.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_db3`
--

-- --------------------------------------------------------

--
-- Table structure for table `Files`
--

CREATE TABLE `Files` (
  `id` int(11) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `path` varchar(500) NOT NULL,
  `mimetype` varchar(100) NOT NULL,
  `size` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `Message`
--

CREATE TABLE `Message` (
  `id` int(11) NOT NULL,
  `sender` varchar(100) NOT NULL,
  `receiver` varchar(100) DEFAULT NULL,
  `message` varchar(300) DEFAULT NULL,
  `time` varchar(40) DEFAULT NULL,
  `file` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) UNSIGNED NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `socketid` varchar(100) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL,
  `admin` enum('true','false') NOT NULL,
  `handledBy` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `socketid`, `username`, `password`, `admin`, `handledBy`) VALUES
(1, 'OBF222BVH806GJJaAAAB', 'Ashik', 'asdf', 'true', NULL),
(2, 'M5r8S24UYjKOGz4wAAAJ', 'david', 'asdf', 'true', NULL),
(3, '/#NIkklpdh2fq-ej6uAAAA', 'john', 'qwerty', 'false', NULL),
(4, '3TPSpI1z7PuJl2c8AAAF', 'ed', 'qwerty', 'false', NULL),
(5, '/#HaNziwakQxE7YXEsAAAo', 'aj', 'qwerty', 'false', ''),
(6, 'Dh_Qy5yLZInyt2s7AAAD', 'test', 'qwerty', 'false', NULL),
(7, 'FCMrkXIdr3n2NumaAAAI', 'james', 'asdf', 'true', NULL),
(8, 'LZGfl_Yt52-KHo8QAAAE', 'joe', 'asdf', 'true', NULL),
(10, '/#lu3nwYxwXfjLRo5aAAAA', 'admin', 'zsxdcfghj', 'true', NULL),
(16, 'AW7kiI0iNETrnYVjAAAb', 'sid', 'zsxdcfghj', 'false', NULL),
(21, 'nrWdMHZ8YEgDaOW0AAAF', 'superadmin', 'zsxdcfghj', 'true', NULL),
(31, 'Z5D4VczvJA12CwA0AAAC', 'smsbhatt', 'zsxdcfghj', 'false', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Files`
--
ALTER TABLE `Files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Message`
--
ALTER TABLE `Message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Files`
--
ALTER TABLE `Files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `Message`
--
ALTER TABLE `Message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
