'use strict';

module.exports = function(socket, conn, io) {


    socket.on('getUsers',function(e,fn){
             conn.query('SELECT username FROM `User` WHERE `admin` = "false"', function(error, results) {
            if (error) {
                console.log("error:", error);
            } else {
                // console.log("FETCHED USERS FROM MYDB");
                // console.log(results);
                fn(results);
            }
        });       
    });

    socket.on('setAdmin',function(input,fn){
        // console.log("setAdmin::",input);
        conn.query('UPDATE `User` SET `handledBy` = ? WHERE `username` = ?',[input.name, input.data],function(error,result){
            if(error){console.log("ERROR SETTING HANDLER",error);}
        })

        socket.broadcast.to('Admins').emit('closePopup', input.data);

        conn.query('SELECT socketid FROM `User` WHERE `username` = ?',[input.data],function(error,result){
            // console.log("result:",result[0].socketid);
            io.to(result[0].socketid).emit('setAdmin',input.name)
        })
        fn("test")
    })

    socket.on('clearHandler',function(input,fn){
        // console.log("clearHandler::",input);
        conn.query('UPDATE `User` SET `handledBy` = ? WHERE `username` = ?',[null,input.name],function(err,result){
            if(err){console.log("ERROR CLEARING HANDLER:",err);}
        })
        fn("cleared handler")
    })

    socket.on('getAdmins',function(input,fn){
        conn.query('SELECT username FROM `User` WHERE `admin` = "true" AND username != ?',[input.name],function(err,result){
            if(err){console.log("ERROR FETCHING ADMINS:",err);}
            else{fn(result)}
        })
    })

    socket.on('transferAdmin',function(input,fn){
        // console.log("TRANSFER ADMIN::"+input.data.user+"-->"+input.data.admin);
        conn.query('UPDATE `User` SET `handledBy` = ? WHERE `username` = ?',[input.data.admin,input.data.user],function(err,result){
            if(err){console.log("ERROR TRANSFERADMIN:",err);}
            conn.query('SELECT socketid FROM `User` WHERE `username` = ?',[input.data.user],function(error,result){
            // console.log("Emit setAdmin-->",result[0].socketid);
            io.to(result[0].socketid).emit('setAdmin',input.data.admin)
            })

        })
        conn.query('SELECT `socketid` FROM `User` WHERE `username` = ?',[input.data.admin],function(err,result){
            if(err){console.log("ERROR:",err);}
            io.to(result[0].socketid).emit('transferCall',input.data.user)
        })
        fn("transferAdmin completed")
    })

    socket.on('getTransferMessages',function(input,fn){
        // console.log('getTransferMessages:',input)
        conn.query('SELECT * FROM `Message` LEFT JOIN `Files` ON Message.file = Files.id WHERE (`sender` = ? OR `receiver` = ?) AND `time` LIKE ?',[input.data.name,input.data.name,'%'+input.data.date+'%'],function(err,result){
            if(err){console.log("ERROR in getTransferMessages:",err);}
            if(result){fn(result)}
        })
        // fn('test')
    })

};