angular.module('app')
.controller('adminController', ['$scope', 'Socket', 'Session', '$state', '$timeout', '$http','Upload', function($scope, Socket, Session, $state, $timeout, $http, Upload) {

    $scope.user = Session.user.username;
    $scope.messages = [];

    $scope.onlineUsers = [];
    $scope.userObj = {};
    $scope.selectedCount = 0;
    $scope.popupMessage = {};

    Socket.emit("getUsers", {}, function(res) {
        console.log("getUsers::::::::::::::::");
        res.forEach(function(u){
            $scope.onlineUsers.push(u.username);
        })
        $scope.onlineUsers.forEach(function(x){
            $scope.userObj[x] = {
            selected: false
        }
        })
        // console.log("GETUSERS RESULT",$scope.onlineUsers);
    })


var getTime = function(){
    var timestamp = moment().valueOf();
    var momentTime = moment.utc(timestamp);
    momentTime = momentTime.local().format('h:mm a DD/MM/YY');
    return momentTime;
}

    // upload on file select or drop
    $scope.upload = function (file,index) {
        // $scope.file = file;
        if(index !=0 || broadcastPopupOpen == false){
        // console.log("inside upload",$scope.file)
        Upload.upload({
            url: '/upload',
            data: {file: file, 'sender': $scope.user, 'receiver': $scope.clickedUser[index], 'time': getTime()}
        }).then(function (resp) {
            // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            console.log(resp)
            var newMessage = {
                sender: $scope.user,
                receiver: $scope.clickedUser[index],
                message: '',
                time: getTime(),
                path: 'uploads/'+resp.data.filename
            }
            console.log("newMessage",newMessage)
            $scope.popupMessage[index].push(newMessage)
            // console.log($scope.popupMessage[0])
            setScrolltop(index)
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });
      }else if(index == 0 && broadcastPopupOpen == true){
            var msg;
            for(var key in $scope.userObj){
                if($scope.userObj[key].selected == true){
                // console.log("inside upload",$scope.file)
                Upload.upload({
                    url: '/upload',
                    data: {file: file, 'sender': $scope.user, 'receiver': key, 'time': getTime()}
                }).then(function (resp) {
                // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                console.log(resp)
                var newMessage = {
                    sender: $scope.user,
                    receiver: key,
                    message: '',
                    time: getTime(),
                    path: 'uploads/'+resp.data.filename
                }
                // msg = {sender: 'broadcast',receiver: key,message: '',time: getTime(),path: 'uploads/'+resp.data.filename}
                msg = newMessage;
                // console.log("popupMessage",$scope.popupMessage[0]);
                // console.log("newMessage",newMessage)
                findIndex(key,function(res){
                    if(res != false){
                    $scope.popupMessage[res].push(newMessage)
                    setScrolltop(res)
                    }
                })
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                // var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
            $timeout(() => {
                msg.sender = 'broadcast'
                $scope.popupMessage[0].push(msg);
            },1000)    

        }
        }
        // $scope.popupMessage[0].push(msg)
        // console.log("popup0:",$scope.popupMessage)
      }
    };


    /*=============================
    =            popup            =
    =============================*/
    console.log("USEROBJ",$scope.userObj);
    $scope.message = [];
    $scope.msg_wrap = [];
    $scope.chat_boxClick = false;
    $scope.msg_boxClick = [];
    $scope.clickedUser = [];
    $scope.firstMessage = [];

    var fm = "";
    var initShow = function(){
        console.log("INSIDE INITSHOW");
        for(var i=0; i < 3; i++){
            $scope.msg_wrap[i] = false;
            $scope.firstMessage[i] = "";
            $scope.chat_boxClick[i] = false;
            $scope.msg_boxClick[i] = false;
            $scope.clickedUser[i] = "";
            $scope.popupMessage[i] = [];
        }
    }
    initShow();
    var broadcastPopupOpen = false;
    $scope.$watch('clickedUser[0]',function(){
        var btn = document.getElementById("broadcast-transfer");
        if($scope.clickedUser[0].indexOf('Selected') != -1){
            broadcastPopupOpen = true;
            btn.style.display = "none";
            // console.log('broadcastPopupOpen::',broadcastPopupOpen);
        }else{
            broadcastPopupOpen = false;
            btn.style.display = "initial";
            // console.log('broadcastPopupOpen::',broadcastPopupOpen);
        }
    })

    var fetchMessage = function(user,index){
        // console.log("fetchMessage");
        Socket.emit("fetchMessage",user,function(res){
            console.log("fetchMessage response:",res);
            // $scope.popupMessage[index] = res;

            function messageFilter(i) {
                return i.sender == $scope.user || i.receiver == $scope.user || i.receiver == 'Admins';
            }

            $scope.popupMessage[index] = res.filter(messageFilter);
            // console.log("popmessage::",$scope.popupMessage[index]);
            setScrolltop(index); 
        })
        return;
    }

    var setScrolltop = function(index){
        // console.log("TYPE INDEX scrollTop:",typeof(index));

        $timeout(() => {
            var container;
            if(typeof(index) === 'number') {
               container = document.getElementById('msg_body'+index);
            }else {
                container = document.getElementById(index);
            }
            if(container){
                console.log("inside Scrolltop");
            container.scrollTop = container.scrollHeight - container.clientHeight;
            }
        });
        return;
    }

    var openPopup = function(user,index){
            fetchMessage(user,index);
            $scope.clickedUser[index] = user;
            $scope.msg_boxClick[index] = true;
            $scope.msg_wrap[index] = true;
            setScrolltop(index);
            if(fm){
                $scope.firstMessage[index] = fm;
                fm = "";
            }
    }

    var findIndex = function(user,cb){
        var matchFound = false;
        for(var i=0; i < 3; i++){
            if($scope.clickedUser[i] == user){
                matchFound = i;
                }
            }
        cb(matchFound);
    }


    var loop = 0;
    var loopBack = function(user) {
        // console.log("inside loopback:",loop);
        if(loop == 0){
            if(broadcastPopupOpen == false){
            openPopup(user,0)
            loop++;
            }
            else{
                loop++;
                $scope.chatbox(user)
            }
        }
        else if(loop == 1){
            openPopup(user,1)
            loop++;
        }
        else if(loop >= 2){
            openPopup(user,2)
            loop = 0;
        }

    }

    $scope.chatbox = function(user){
        findIndex(user,function(res){
        // console.log("index:",res);
        if(res === false){
            // console.log("index false");
            if($scope.clickedUser[0] == "" && broadcastPopupOpen == false){
                openPopup(user,0)
            }
            else if($scope.clickedUser[1] == ""){
                openPopup(user,1)
            }
            else if($scope.clickedUser[2] == ""){
                openPopup(user,2)
            }
            else if(res == false){
                loopBack(user)
            }
        }
        else {
            $scope.msg_wrap[res] = !($scope.msg_wrap[res])
        }
        });
    }
    
    $scope.selection = function(e,user){
        e.stopPropagation();
        if($scope.selectedCount == 0){
            // initShow();
            $scope.close(0);
            loop++;
        }
        if($scope.userObj[user].selected){
            $scope.userObj[user].selected = false;
            $scope.selectedCount -= 1;
                    if($scope.selectedCount == 0){
                        // initShow();
                        $scope.close(0)
                        }
        }else {
            $scope.userObj[user].selected = true;
            $scope.selectedCount += 1;
        }

        if($scope.selectedCount){
            console.log("INSIDE 1")
            $scope.clickedUser[0] = $scope.selectedCount + " Selected";
            $scope.msg_boxClick[0] = true;
            $scope.msg_wrap[0] = true;
         }//else {
        //     console.log("INSIDE2")
        //     $scope.clickedUser[0] = user;
        //     $scope.msg_boxClick[0] = true;
        //     $scope.msg_wrap[0] = true;
        // }
        // console.log($scope.userObj);
        console.log($scope.selectedCount,"selected");

    }

    $scope.close = function(index){
        if(index == 0 && broadcastPopupOpen == true){clearSelection()}
        $scope.msg_boxClick[index] = !$scope.msg_boxClick[index];
        $scope.msg_wrap[index] = !$scope.msg_wrap[index]; 
        $scope.clickedUser[index] = "";
        $scope.firstMessage[index] = "";
        $scope.popupMessage[index] = [];
        onClose(index);
    }

    var onClose = function(index){
        // console.log("inside onclose");
        if(index == 0){
            if($scope.clickedUser[1] || $scope.clickedUser[2]){
                if($scope.clickedUser[1]){
                    openPopup($scope.clickedUser[1],0)
                    $scope.msg_wrap[index] = !$scope.msg_wrap[index]; 
                    $scope.close(1)}
                if($scope.clickedUser[2]){
                    openPopup($scope.clickedUser[2],1)
                    $scope.msg_wrap[index] = !$scope.msg_wrap[index]
                    $scope.close(2)}
            }
        }else if(index == 1){
            if($scope.clickedUser[2]){
                openPopup($scope.clickedUser[2],1)
                $scope.msg_wrap[index] = !$scope.msg_wrap[index]; 
                $scope.close(2)
            }
            }
    }

    $scope.selectall = function(){
        for(prop in $scope.userObj){
            $scope.userObj[prop].selected = true;
            // console.log("HERE");
        }
        $scope.selectedCount = Object.keys($scope.userObj).length;
        $scope.clickedUser[0] = $scope.selectedCount + " Selected";
        $scope.firstMessage[0] = false;
        $scope.msg_boxClick[0] = true;
        $scope.msg_wrap[0] = true;
        console.log("SELECTEDCOUNT",$scope.selectedCount,$scope.userObj);
    }
    var clearSelection = function(){
        for(prop in $scope.userObj){
        $scope.userObj[prop].selected = false;
        }
        $scope.selectedCount = 0;
    }

    $scope.selectnone = function(){
        clearSelection();
        if(broadcastPopupOpen == true){
            $scope.close(0);
            loop++;
        }
        // console.log("SELECTEDCOUNT",$scope.selectedCount,$scope.userObj);
    }

    $scope.handle = function(res,index){
        $scope.firstMessage[index] = "";
        if(res == 'yes'){
            // document.getElementById("msg_handle"+index).style.display = 'none';
            Socket.emit('setAdmin',$scope.clickedUser[index],function(res){})
        }else {
            $scope.close(index);
        }
    }

    Socket.on('closePopup',function(user){
        for(var i =0; i < 3; i++){
            if($scope.clickedUser[i] == user){
                $scope.close(i);
            }
        }
    })
    
    
    $scope.Admins = [];
    Socket.emit('getAdmins',{},function(res){
            console.log("ADMINS:",res);
            $scope.Admins = res;
    });

    $scope.showAdmins = function(e,index){
        e.stopPropagation();
        $('#dropdownMenuButton'+index).trigger('click.bs.dropdown');
        console.log("clicked");
    }

    $scope.transferAdmin = function(admin,index){
        var transferAdmin = {admin: admin , user: $scope.clickedUser[index]}
        console.log("transfer:"+transferAdmin.user+"-->"+transferAdmin.admin);
        Socket.emit('transferAdmin', transferAdmin,function(res){
            console.log(res);
        })
        $scope.close(index);
    }

    Socket.on('transferCall',function(user){
        console.log("got a transfer call from ",user);
        var timestamp = moment().valueOf();
        var momentTime = moment.utc(timestamp);
            momentTime = momentTime.local().format('DD/MM/YY');
        var fetchData = {
            name: user,
            date: momentTime
        }
        Socket.emit('getTransferMessages',fetchData,function(result){
            console.log("transfer msg response:",result);
            $scope.chatbox(user);
            pushMessage(user,result);

        })
    })

    var pushMessage = function(user,msg){
        $timeout(() => {
        for(var i = 0; i < 3; i++){
            if($scope.clickedUser[i] == user){
                // console.log("pushed Msg:",i," msg:",msg);
                // console.log("Before",$scope.popupMessage[i]);
                for(var j = 0; j < msg.length; j++){
                    $scope.popupMessage[i].push(msg[j])
                }
                setScrolltop(i);
                // console.log("After:",$scope.popupMessage[i]);
            }
        } },2000);
    }
    
    /*=====  End of popup  ======*/


    if($scope.user == "superadmin"){
        // console.log("MAIN ADMIN");
        
        Socket.emit('getMessages', {}, function(messages) {
            // console.log('monitor Messages:', messages)
            $scope.messages = messages;
            setScrolltop('messageContainer')
    })
        
    }

    $scope.disconnect = function() {
        Session.user = '';
        $http.get('/logout').then(function(res){
            $state.go('login')
            console.log("LOGGED OUT")});}

    $scope.inputMessage = function(event,user,index){
        var key = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
        // console.log("Event:",key,"user:",user,"index:",index);
        if(key == 13 && $scope.message[index] != ""){
            console.log("MESSAGE:::",user,"-->",$scope.message[index]);
            
            var timestamp = moment().valueOf();
            var momentTime = moment.utc(timestamp);
            momentTime = momentTime.local().format('h:mm a DD/MM/YY');

            if(broadcastPopupOpen == true && index == 0){
                // var selecteduser = "";
                    var newMessage = {};
                for(var key in $scope.userObj){
                    if($scope.userObj[key].selected == true){
                        // selecteduser = key;
                        var newMessage = {
                            sender: $scope.user,
                            receiver: key,
                            message: $scope.message[index],
                            time: momentTime
                        }
                        findIndex(key,function(res){
                            if(res != false){$scope.popupMessage[res].push(newMessage)}
                        })
                        
                    Socket.emit("chatMessage", newMessage, function(response) {
                        // console.log("EMITTING DATA MSG::",newMessage);
                    })
                    }
                }
                newMessage = {sender: "broadcast",receiver: $scope.selectedCount,message: $scope.message[index],time: momentTime}
                // console.log("broadcast push msg::",index,newMessage);
                $scope.popupMessage[index].push(newMessage) 
                $scope.message[index] = "";
                setScrolltop(index);
            }else {
                var newMessage = {
                    sender: $scope.user,
                    receiver: user,
                    message: $scope.message[index],
                    time: momentTime
                    }

                Socket.emit("chatMessage", newMessage, function(response) {
                    console.log("EMITTING DATA MSG::",newMessage);

                })
                $scope.message[index] = "";
                $scope.popupMessage[index].push(newMessage)      //testing
                // if($scope.user != "Ashik"){$scope.popupMessage[index].push(newMessage)}   // testing line
                setScrolltop(index);
            }


        }
    }
    Socket.on('monitorMessage',function(message){
        $scope.messages.push(message.data);
        setScrolltop('messageContainer')
    })

    $scope.setOnline = {};
    Socket.on('onlineUsers',function(input){
        console.log("online users =",input)
        for(var i=0; i < $scope.onlineUsers.length; i++){

            $scope.setOnline[$scope.onlineUsers[i]] = false;
        }
        for(var j=0; j < input.length; j++){
            $scope.setOnline[input[j]] = true;
        }
    })
    Socket.on('chatMessage', function(message) {
        var boxOpen = false;
        console.log("INCOMING MSG",message);
        // $scope.messages.push(message.data);
        for(var i=0; i < 3; i++){
            if($scope.clickedUser[i] == message.data.sender || $scope.clickedUser[i] == message.data.receiver){
                boxOpen = true;
                console.log("HERE AGAIN::",message.data.receiver);
                if(message.data.receiver == 'Admins'){
                    $scope.firstMessage[i] = message.data.message;
                    // console.log("FIRSTMESSAGE:");
                    // console.log("scope.firstMessage"+i+"::",$scope.firstMessage[i]);
                }
                $scope.popupMessage[i].push(message.data)
                console.log("THis is the data:",message.data);
                setScrolltop(i);
            }
        }
        if(boxOpen == false){
            console.log("open box,",message.data);
            if(message.data.receiver == 'Admins'){fm = message.data.message ? message.data.message : message.data.path }
            console.log("has to open::",message.data.sender);
            $scope.chatbox(message.data.sender);
        }
        setScrolltop('messageContainer');
    })


}]);