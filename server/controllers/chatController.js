'use strict';

module.exports = function(socket, conn, io, app, upload) {

app.post('/upload', upload.single('file'), function (req, res, next) {
    console.log("data:",req.body.sender);
    console.log("data:",req.body.receiver);
    console.log("data:",req.file.path);
    req.file.path = '/uploads/'+req.file.filename
    conn.query("INSERT INTO `Files` SET `filename` = ?, `path` = ?, `mimetype` = ?, `size` = ?",[
        req.file.filename,
        req.file.path,
        req.file.mimetype,
        req.file.size],function(err,result){
            if(err){console.log(err)}
        var file_id = result.insertId;
        conn.query("INSERT INTO `Message` SET `sender` = ?, `receiver` = ?,`time` = ?, `file` = ?",[
            req.body.sender,
            req.body.receiver,
            req.body.time,
            file_id
        ],function(err,rslt){
            if(err){console.log(err)}
            else{
                var message = {
                    data: {
                    sender: req.body.sender,
                    receiver: req.body.receiver,
                    message: '',
                    time: req.body.time,
                    path: 'uploads/'+req.file.filename
                }
                }
                conn.query('SELECT `socketid` FROM `User` WHERE `username` = ?',["superadmin"],function(err,res){
                    if(err){console.log("ERROR FETCHING SOCKETID FOR MONITOR:",err);}
                    if(res){io.to(res[0].socketid).emit('monitorMessage',message)}
                })
                if(req.body.receiver != 'Admins'){
                conn.query('SELECT `socketid` FROM `User` WHERE `username` = ?',[req.body.receiver],function(err,result){
                    if(result){
                        console.log("SOCKETID:::",result[0].socketid);

                      io.to(result[0].socketid).emit('chatMessage', message);  
                    }
                })}else {
                    socket.broadcast.to('Admins').emit('chatMessage',message)
                }

              res.send(req.file)
                }
        })
    })
})

    function addMessage(message, cb) {
        console.log("CHAT MESSAGE::",message.data);
        conn.query("INSERT INTO Message SET ?", [message.data], function(err, result) {
            if (err) {
                console.log("Error addMessage:", err);
            } else {
                console.log("CHAT SAVED IN MYDB");
                cb('success');
            }
        });
    }

    function findOnlineUsers(cb){
        var onlineusers = [];
        // var length = Object.keys(io.sockets.sockets).length
        // var count = 0;
        conn.query('SELECT * FROM User WHERE admin = ?',['false'],function(err,result){
            if(err){
                console.error('ERROR!inside::::::::::' + err);
            }
            else{
                // console.log("result:",result);
                for(var i=0; i < result.length; i++){
                    Object.keys(io.sockets.sockets).forEach(function(id){
                        if(result[i].socketid == id){
                            onlineusers.push(result[i].username)
                            console.log("inside keys:",onlineusers);
                        }
                    })
                }
                cb(onlineusers)
            }
        })
        }




    socket.on('initSocket', function(user) {
        
        // console.log("INITSOCKET DATA",user);
        // console.log("NEW SOCKET ID::" + socket.id);
        // var count = io.engine.clientsCount;
        // console.log("online users count:",count);
        
        if(user.admin == 'true'){
            socket.join('Admins')
            console.log("joined room Admins");
        }else if(user.admin == 'false'){
            socket.join('Users')
            console.log("joined room Users");
        }
        conn.query("UPDATE User SET socketid = ? WHERE username = ?", [socket.id, user.username], function(err, result) {
            if (err) {
                console.error('ERROR!::::::::::' + err);
            } else {
                console.log("SOCKETID CHANGED IN MYDB");
                findOnlineUsers(function(res){
                   console.log("users nline:",res);
                   socket.broadcast.to('Admins').emit('onlineUsers',res) 
                })

            }
        });
    }) 
    
    console.log("Connected");

    
    
    socket.on('fetchMessage',function(message,fn){
        console.log("fetchMessage::",message);
        conn.query('SELECT * FROM `Message` LEFT JOIN `Files` ON Message.file = Files.id WHERE `sender` = ? OR `receiver` = ?',[message.data,message.data],function(err,result){
            fn(result);
        })
        
    })



    socket.on('chatMessage', function(message, fn) {
        console.log("SERVER SOCKET EMIT MSG::",message);
        addMessage(message, function(response) {
            if (response == 'success') {
                    // console.log("EMITING TO MONITOR::",monitor);
                    // socket.broadcast.to('monitor').emit('monitorMessage',message)
                    conn.query('SELECT `socketid` FROM `User` WHERE `username` = ?',["superadmin"],function(err,result){
                        if(err){console.log("ERROR FETCHING SOCKETID FOR MONITOR:",err);}
                        if(result){io.to(result[0].socketid).emit('monitorMessage',message)}
                    })
                    if(message.data.receiver == 'Admins'){
                        socket.broadcast.to('Admins').emit('chatMessage',message)
                    }else {
                        conn.query('SELECT `socketid` FROM `User` WHERE `username` = ?',[message.data.receiver],function(error,result){
                            if(result){
                                console.log("SOCKETID:::",result[0].socketid);
                              io.to(result[0].socketid).emit('chatMessage', message);  
                            }
                        })
                    }
                // socket.broadcast.emit('chatMessage', message)
                fn('success');
            } else {
                fn('error');
            }
        })
    })
    
    socket.on('getMessages', function(input, fn) {
            console.log("getMEssages INPUT::",input);
            if(input.name == "superadmin"){
                    conn.query('SELECT * FROM `Message` LEFT JOIN `Files` ON Message.file = Files.id',function(error,result){
                    if(error){
                       console.log("error:", error); 
                   } else {
                        console.log("FETCHED MESSAGES FOR MONITORING");
                        fn(result);
                   }
                })
            }

            if(input.admin == 'false'){
                conn.query('SELECT * FROM `Message` LEFT JOIN `Files` ON Message.file = Files.id WHERE `receiver` = ? OR `sender` = ?',[input.name,input.name], function(error, results) {
                    if (error) {
                    console.log("error:", error);
                    fn(error);
                    } else {
                    console.log("FETCHED MSGS FROM MYDB");
                    // console.log(results);
                    fn(results);
                    }
                });
            } 


    })
    
    socket.on('logout', function(user, cb) {
        console.log("DISCONNECT Data::",user);
        if(user.admin == 'true'){
            socket.leave('Admins')
            console.log("leaved room Admins");
        }else if(user.admin == 'false'){
            socket.leave('Users')
            console.log("leaved room Users");
        }
        console.log('disconnect')
        cb();
    })
}